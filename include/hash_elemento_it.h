#include"../include/hashtable.h"
typedef struct{
	hashtable* ht;
	unsigned int indice;
	objeto* elem;
}hash_elemento_it;

#define HT_ITERATOR(ht) {ht, 0, ht->buckets[0]}

char* iterador_claves(hash_elemento_it* it);
objeto* ht_iterador(hash_elemento_it* it);
void clear(hashtable* tabla,int free_d);


