#include<stdio.h>
#include"../include/hashtable.h"
#include<stdlib.h>
char **claves(hashtable *tabla, int *conteo){
      	if(tabla->elementos==0){
		*conteo = 0;
		return NULL;
	}
	char **hashtable_keys = (char **)malloc((tabla->numeroBuckets)*sizeof(tabla->buckets));
	objeto *temp = NULL;
	int index=0;
	int cont1=0;

	for(int i=0;i<=tabla->numeroBuckets;i++){
		temp = tabla->buckets[i];
		while(temp){
			hashtable_keys[index++] = temp->clave;
			temp = temp->siguiente;
			cont1++;		
		}
	}
	*conteo = cont1;
	return hashtable_keys;
}
