#include<stdio.h>
#include<stdlib.h>
#include"../include/hashtable.h"
hashtable *crearHashTable(int numeroBuckets){
	hashtable *tabla=malloc(sizeof(hashtable));
	int i;

	if(numeroBuckets<1)
		return NULL;

	if((tabla=malloc(sizeof(hashtable)))==NULL){
		return NULL;
	}
	

	if((tabla->buckets=malloc(sizeof(objeto*)*numeroBuckets))==NULL){
		free(tabla->buckets);			
		return NULL;

	}
	tabla->numeroBuckets=numeroBuckets;
	tabla->elementos=0;
	
	for(i=0;i<numeroBuckets;i++){
		tabla->buckets[i]=NULL;

	}

	
	return tabla;
}


