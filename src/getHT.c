#include<stdio.h>
#include "../include/hashtable.h"


void *get(hashtable *tabla, char *clave){

	unsigned long ind=hash((unsigned char*) clave)%tabla->numeroBuckets;
        objeto *temp = tabla->buckets[(int)ind];
	while(temp!=NULL){

		if(!strcmp(temp->clave,clave)){

			return temp->valor;
		}
		temp = temp->siguiente;
	}
	return NULL;

}
