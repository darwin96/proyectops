#include<stdio.h>
#include"../include/hashtable.h" 
#include<stdlib.h>

void **valores(hashtable *tabla, int *conteo){
	if(tabla->elementos==0){
		*conteo = 0;
		return NULL;
	}
	void **hashtable_valores=(void **)malloc((tabla->elementos)*sizeof(tabla->buckets));	
	objeto *temp = NULL;
	int index=0;
	int cont1;

	for(int i=0;i<=tabla->numeroBuckets;i++){
		temp = tabla->buckets[i];
		while(temp){
			hashtable_valores[index++] = temp->valor;
			temp = temp->siguiente;
			cont1++;	
		}
	}
	*conteo = cont1;
	return hashtable_valores;



}
