#include<stdio.h>
#include"../include/hashtable.h"
#include<string.h>
#include<stdlib.h>
void *remover(hashtable *tabla, char *clave){

	unsigned long hs = hash((unsigned char*)clave)%tabla->numeroBuckets;
	objeto *p = tabla->buckets[(int)hs];
	objeto *prev = NULL;
	while(p != NULL){
		if(!strcmp(p->clave,clave)){
			void *temp = p->valor;
			if(prev != NULL)
				prev->siguiente = p->siguiente;
			else 
				tabla->buckets[hs] = p->siguiente;
			free(p);
			p=NULL;
			tabla->elementos--;
			return temp;
		}	
		prev = p;
		p = p->siguiente;
	}

	return NULL;
}
