#Escriba su makefiile en este archivo
#
# Integrante 1 Darwin Guaman Pacalla
# Integrante 2 Efren Parra 

CC=gcc
CGF=-Wall -c -g -I include/

programa: prueba.o put.o get.o calcHash.o crearHT.o contar.o cont_clave.o claves.o valores.o remove.o clear.o ht_iterador.o iterador_claves.o borrarHT.o 
	gcc obj/prueba.o obj/put.o obj/get.o obj/calcHash.o obj/crearHT.o obj/contar.o obj/cont_clave.o obj/claves.o obj/valores.o obj/remove.o obj/clear.o obj/ht_iterador.o obj/iterador_claves.o obj/borrarHT.o -o bin/programa

ht_iterador.o:src/ht_iterador.c
	$(CC) $(CGF) src/ht_iterador.c -o obj/ht_iterador.o
claves.o:src/claves.c
	$(CC) $(CGF) src/claves.c -o obj/claves.o
clear.o:src/clear.c
	$(CC) $(CGF) src/clear.c -o obj/clear.o
cont_clave.o:src/contieneClave.c
	$(CC) $(CGF) src/contieneClave.c -o obj/cont_clave.o
ht_iterator.o:src/ht_iterator.c
	$(CC) $(CGF) src/contieneClave.c -o obj/ht_iterator.o
iterador_claves.o:src/iterador_claves.c
	$(CC) $(CGF) src/iterador_claves.c -o obj/iterador_claves.o
remove.o:src/removefunction.c
	$(CC) $(CGF) src/removefunction.c -o obj/remove.o
valores.o:src/valores.c
	$(CC) $(CGF) src/valores.c -o obj/valores.o
crearHT.o:src/crearHT.c 
	$(CC) $(CGF) src/crearHT.c -o obj/crearHT.o	
prueba.o:src/prueba.c
	$(CC) $(CGF) src/prueba.c -o obj/prueba.o
put.o:src/put.c 
	$(CC) $(CGF) src/put.c -o obj/put.o
get.o:src/getHT.c
	$(CC) $(CGF) src/getHT.c -o obj/get.o
calcHash.o:src/calcHash.c
	$(CC) $(CGF) src/calcHash.c -o obj/calcHash.o
contar.o:src/contarElementos.c
	$(CC) $(CGF) src/contarElementos.c -o obj/contar.o
borrarHT.o:src/borrarHT.c
	$(CC) $(CGF) src/borrarHT.c -o obj/borrarHT.o

clean: 
	rm bin/* obj/* 

